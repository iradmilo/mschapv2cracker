import hashlib
import time
from collections import defaultdict
from multiprocessing import cpu_count, Manager, Pool
import multiprocessing
import os
import sys
import threading
import codecs
import json
from tqdm import tqdm
from itertools import islice

targetFormat = 'utf-8'


class Spinner:
    busy = False
    delay = 0.1

    @staticmethod
    def spinning_cursor():
        while 1:
            for cursor in '|/-\\': yield cursor

    def __init__(self, delay=None):
        self.spinner_generator = self.spinning_cursor()
        if delay and float(delay): self.delay = delay

    def spinner_task(self):
        while self.busy:
            sys.stdout.write(next(self.spinner_generator))
            sys.stdout.flush()
            time.sleep(self.delay)
            sys.stdout.write('\b')
            sys.stdout.flush()

    def start(self):
        self.busy = True
        threading.Thread(target=self.spinner_task).start()

    def stop(self):
        self.busy = False
        time.sleep(self.delay)


def nt_password_hash(data, return_list, numprocess):
    dict_ = defaultdict(list)
    # for password in [*data]:
    for password in tqdm([*data], position=numprocess, miniters=1, mininterval=0.5, desc=str(numprocess)):
        pwd_bin = str.encode(password, encoding='utf-16')[
                  2:-2]  # [2:-2] for ISO-8859-1 format or rock_you file else [2:] for txt file
        md4 = hashlib.new('md4')
        md4.update(pwd_bin)
        hash_value = md4.digest().hex()[28:32]
        dict_[hash_value].append(password[:-1])
    return_list.append(dict_)


def slice_(file, N):
    while True:
        chunk = tuple(islice(file, N))
        if not chunk:
            return
        yield chunk


def write_to_file(item):
    filename = os.path.join(g_output_dir, '{key}.json'.format(key=item[0]))
    with codecs.open(filename, 'a', 'utf8') as f:
        str_ = json.dumps(item[1])
        f.write(str_)
        f.write(os.linesep)


def init(output_dir):
    global g_output_dir
    g_output_dir = output_dir


if __name__ == '__main__':

    multiprocessing.freeze_support()
    # filename = sys.argv[1]
    # size = int(sys.argv[2])
    SIZE = 1000000
    filename = "cleanfile.txt"
    num_process = cpu_count()

    OUTPUT_DIR_NAME = 'Files'
    FILE_DIR = os.path.dirname(os.path.realpath('__file__'))
    OUTPUT_DIR = os.path.join(FILE_DIR, '{output_dir}'.format(output_dir=OUTPUT_DIR_NAME))
    os.makedirs(OUTPUT_DIR, exist_ok=True)

    start = time.time()
    ######################################################

    print("Loading dictionary...")
    start_r = time.time()
    spinner = Spinner()

    encodings = ['utf-8', 'Windows-1252', 'ISO-8859-1', 'ansi']
    data = False
    error_reading_file = False

    manager = Manager()
    return_list = manager.list()
    br = -1

    for encoding in encodings:
        try:
            with open(filename, 'r', encoding=encoding) as file:
                # data = f.readlines()
                # with  Pool(num_process) as pool:
                '''
                Loading dictionary in chunks and
                Calculating num_process dictionaries on num_process processes
                '''
                print("Loading dictionary and Calculating on ", num_process, "processes...")
                start_c = time.time()
                spinner.start()
                pool1 = Pool(num_process)
                for data in slice_(file, SIZE):
                    br = br + 1
                    pool1.apply_async(nt_password_hash, args=(data, return_list, br))
                pool1.close()
                pool1.join()
                spinner.stop()
                end_r = time.time()
                print("Loading dictionary and Calculating time: ", end_r - start_r, "s\n")

        except FileNotFoundError as error:
            error_reading_file = error
        except UnicodeDecodeError as error:
            print('Got unicode error with {encoding} , trying different encoding'.format(encoding=encoding))
            if encoding != encodings[-1]:
                pass
            else:
                error_reading_file = error
        except IOError as error:
            error_reading_file = error

        else:

            print('File successfully read. Encoding: {encoding}. Dictionaries calculated. Encoding: {encoding}'.format(
                encoding=encoding))
            break

    if error_reading_file:
        spinner.stop()
        print(error_reading_file)
        os._exit(1)

    '''
    Merging calculated dictionaries
    '''
    print("Merging...")
    start_m = time.time()
    spinner.start()
    final_dict = defaultdict(list)

    # for dict in return_list:
    #     for key, list_ in dict.items():
    #         final_dict[key].extend(list_)

    [final_dict[key].extend(list) for dict in return_list for key, list in dict.items()]

    return_list = []

    spinner.stop()
    end_m = time.time()
    print("Merging time: ", end_m - start_m, "s\n")
    # print(final_dict["89ae"])

    '''
    Writing to file
    '''
    print("Writing to file...")
    start_w = time.time()
    error_saving_file = False
    spinner.start()
    try:
        pool = Pool(num_process * 3, initializer=init, initargs=(OUTPUT_DIR,))
        pool.map(write_to_file, final_dict.items())
        pool.close()
        pool.join()
    except IOError as error:
        error_saving_file = error
    except MemoryError as error:
        error_saving_file = error
    except Exception as error:
        error_saving_file = error
    else:
        ##########################################################
        spinner.stop()
        endW = time.time()
        print("Writing to file execution time: ", endW - start_w, "s\n")
        spinner.stop()
        final_dict.clear()
        end = time.time()
        print("Program execution time: ", end - start, "s")

    if error_saving_file:
        print(error_saving_file)
        os._exit(1)

# with codecs.open("{filename}output.json".format(filename=filename[:filename.rfind(".")]),'w','utf8') as f:
#    str_ = json.dumps(final_dict, separators=(',', ': '), ensure_ascii=False)
#    f.write(str_)
