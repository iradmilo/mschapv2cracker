import csv
import re

def parse_hostapd_wpe():
    handshakeList = []
    iData = {}
    with open('hostapd-wpe.log', 'r') as handshakeFile:
        reader = csv.reader(handshakeFile, delimiter = ' ', skipinitialspace = True)
        for row in handshakeFile:
            if not row.isspace():
                property, data = "".join(row.strip().split()).split(':', 1)
                if property == 'username':
                    iData[property] = data
                elif property == 'challenge' or property == 'response':
                    if isinstance(data, str):
                        data = re.sub(r'[^\da-fA-F]+', '', data)
                    iData[property] = data
                elif property == 'jtrNETNTLM':
                    handshakeList.append(iData)
                    iData = {}
    return handshakeList
