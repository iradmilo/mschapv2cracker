import os
import hashlib
from passlib.crypto import des
from Crypto.Cipher import DES
import time
import multiprocessing
from multiprocessing import Pool, cpu_count
import re
import argparse
import json
from parser_hostapd import parse_hostapd_wpe as hostapd

def nt_password_hash(password):
    '''
    IN              string password
    OUT             MD4 hash binary stream 
    '''
    pwd_bin = password.encode('utf-16')[2:]
    md4 = hashlib.new('md4')
    md4.update(pwd_bin)
    return b''.join((md4.digest(), b'\x00'*5))

def check_password_async_mp(list_chunk, plain, cipher):
    
    for password in list_chunk:
        pwd_hash = nt_password_hash(password)

        key = des.expand_des_key(pwd_hash[:7])
        des_new = DES.new(key, 1)  # 1 is ECB_MODE
        response1 = des_new.encrypt(plain)

        key = des.expand_des_key(pwd_hash[7:14])
        des_new = DES.new(key, 1)  # 1 is ECB_MODE
        response2 = des_new.encrypt(plain)

        if response1 == cipher[:8] and response2 == cipher[8:16]:
            return password

def check_password(password, plain, cipher):
   
    pwd_hash = nt_password_hash(password)   

    key = des.expand_des_key(pwd_hash[:7])
    des_new = DES.new(key, 1)# 1 is ECB_MODE
    response1 = des_new.encrypt(plain)

    key = des.expand_des_key(pwd_hash[7:14])
    des_new = DES.new(key, 1)# 1 is ECB_MODE
    response2 = des_new.encrypt(plain)
    
    if response1 == cipher[:8] and response2 == cipher[8:16]:
        return password

def check_tail(list_chunk, plain, cipher):
    for num in list_chunk:
        hash_value = b''.join((num.to_bytes(2, 'little'), b'\x00'*5))
        key = des.expand_des_key(hash_value)
        des_new = DES.new(key, 1)# 1 is ECB_MODE
        response3 = des_new.encrypt(plain)
        if response3 == cipher[16:]:
            return hash_value

#divide data on chunks
def divide(list_, chunks=1):
    n = len(list_) // chunks
    i = -1
    for i in range(chunks-1):
        yield list_[i*n: (i+1)*n]
    yield list_[(i+1)*n:len(list_)]

class Worker():
    def __init__(self, workers, initargs):
        self.pool = Pool(processes=workers)
        self.workers = workers
        self.plain = initargs[0]
        self.cipher = initargs[1]
        self.result = False

    def callback(self, result):
        if result:
            self.pool.terminate()
            self.result = result
            
    def do_job(self, function, list_):
        for list_chunk in divide(list_, self.workers):
            self.pool.apply_async(function, args=(list_chunk, self.plain, self.cipher), callback=self.callback)
        self.pool.close()
        self.pool.join()

if __name__ == '__main__':

    fullstart = time.time()
    multiprocessing.freeze_support()
    INPUT_DIR_NAME = 'DictionaryFiles'
   
    for idx, item in enumerate(hostapd()):

        username = item['username']
        challenge = item['challenge']
        response = item['response']
        
        print("\n------------------------------------------------------------")
        print("Working on challenge - response no ", idx + 1)
        print("challenge:", challenge)
        print("response:", response)

        plain = bytes.fromhex(challenge)
        cipher = bytes.fromhex(response)
        
        #number of processes
        num_process = cpu_count()

        '''
        Brute forcing values for last 7 bytes of 21 byte nt_password_hash from 0 to 65535(last 5 bytes are 0)
        hash_value -> first 2 bytes from those 7 bytes
        '''


        w1 = Worker(num_process, initargs=(plain, cipher))#parallel on num_processes cores
        w1.do_job(check_tail, range(0, 65536))

        if not w1.result:
            print("Wrong challenge response pair ", challenge, " ", response)
            continue
        else:

            hash_value = w1.result.hex()[0:4]


        '''
        Second part loads data from file {hash_value}.json to get list of passwords 
        which have matching last 7 bytes for their nt_password_hash
        '''

        FILE_DIR = os.path.dirname(os.path.realpath('__file__'))
        INPUT_DIR = os.path.join(FILE_DIR, '{input_dir_}'.format(input_dir_=INPUT_DIR_NAME))
        os.makedirs(INPUT_DIR, exist_ok=True)
        filename = os.path.join(INPUT_DIR, '{key}.json'.format(key=hash_value))

        found = False
        with open(filename, encoding="utf8") as input_file:
            # list_key_is_hash_value = [json.loads(line) for line in input_file]
            # list_key_is_hash_value = [item for sublist in list_key_is_hash_value for item in sublist]  # to flatten lists
            for line in input_file:
                list_key_is_hash_value = json.loads(line)

                if len(list_key_is_hash_value) <= 500:#check in which a case is better to have multiple processes so that IPC is not overkill
                    result = next((password for password in list_key_is_hash_value if check_password(password, plain, cipher)), None)
                    if result:
                        print("\nPassword is", result, end="")
                        found = True
                        break
                else:
                    w = Worker(num_process, initargs = (plain, cipher))  # parallel on num_processes cores
                    w.do_job(check_password_async_mp, list_key_is_hash_value)
                    if (w.result):
                        print("\nPassword is", w.result, end="")
                        found = True
                        break
        if not found:
            print("\nPassword was not found", end="")
            
        if username:
                print(" for a username", username, ".")
        print("------------------------------------------------------------")    

    fullend = time.time()
    print("Full program execution time: ", fullend - fullstart, "s")    
