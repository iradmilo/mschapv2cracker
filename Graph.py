import os
import json
from collections import defaultdict
import numpy
import matplotlib.pyplot as plt

#*args -> x key, x_down_lim, x_up_lim, x_axis_label
def graph(INPUT_DIR, labels, key, y_axis_label, y_down_lim, y_up_lim, *args):
    try:
        i = 0
        for label in labels:
            filename = os.path.join(INPUT_DIR, '{label}.json'.format(label=label))
            with open(filename, encoding="utf8") as input_file:
                 jsonfile = json.load(input_file)
                 if jsonfile[0] is None:
                     print("File is empty: ", filename)
                     continue
                 dict = defaultdict(list)
                 i = i + 0.2
                 for items in jsonfile:
                     for item in items.items():
                        dict[item[0]].append(item[1])
                 average = round(numpy.mean(dict[key]), 5)

                 if len(args):
                    plt.scatter(dict[args[0]], dict[key], s=50 , label=label + ' Avg = {average}s'.format(average=average))
                 else:
                    plt.plot(len(dict[key])*[i], dict[key], "x", label=label + ' Avg = {average}s'.format(average=average))
                 
                 # Place a legend above this subplot
                 plt.legend(loc = 'lower left', bbox_to_anchor = (0., 1.0), ncol=1, mode="expand", borderaxespad=0.)

                 plt.ylabel(y_axis_label, fontsize=14)

                 if len(args):
                    x_down_lim = args[1]
                    x_up_lim = args[2]
                    x_axis_label = args[3]

                    plt.axis([x_down_lim, x_up_lim, y_down_lim, y_up_lim])
                    plt.xlabel(x_axis_label, fontsize=14)
                    graph_path = os.path.join(INPUT_DIR, '{x}{y}.png'.format( x=str(x_axis_label), y=str(y_axis_label)))
                    plt.savefig(graph_path, bbox_inches='tight')
                 else:
                     x_down_lim = 0
                     x_up_lim = (len(labels) + 1) * 0.2
                     plt.ylim(y_down_lim, y_up_lim)
                     plt.xlim(x_down_lim, x_up_lim)
                     graph_path = os.path.join(INPUT_DIR, '{y}.png'.format(y=y_axis_label))
                     plt.savefig(graph_path, bbox_inches='tight')
        plt.clf()#clean plot
    except IOError as error:
            print(error)
            os._exit(1)
