import os, fnmatch
import hashlib
import time
import random
import codecs
import threading
import sys
from multiprocessing import Pool, cpu_count, Manager, Process, Queue, freeze_support
from PeerResponseChallenge import peer_response_challenge
from Graph import graph
from passlib.crypto import des
from Crypto.Cipher import DES
import json
from itertools import islice

def nt_password_hash(password):
    '''
    IN              string password
    OUT             MD4 hash binary stream 
    '''
    pwd_bin = password.encode('utf-16')[2:]
    #[2:-2] for ISO-8859-1 format or rock_you file else [2:] for txt file
    md4 = hashlib.new('md4')
    md4.update(pwd_bin)
    return b''.join((md4.digest(), b'\x00'*5))

def check_password_async_mp(list_chunk, plain, cipher):
    
    for password in list_chunk:
        pwd_hash = nt_password_hash(password)

        key = des.expand_des_key(pwd_hash[:7])
        des_new = DES.new(key, 1)  # 1 is ECB_MODE
        response1 = des_new.encrypt(plain)

        key = des.expand_des_key(pwd_hash[7:14])
        des_new = DES.new(key, 1)  # 1 is ECB_MODE
        response2 = des_new.encrypt(plain)

        if response1 == cipher[:8] and response2 == cipher[8:16]:
            return password

def check_password(password, plain, cipher):

    pwd_hash = nt_password_hash(password)

    key = des.expand_des_key(pwd_hash[:7])
    des_new = DES.new(key, 1)  # 1 is ECB_MODE
    response1 = des_new.encrypt(plain)

    key = des.expand_des_key(pwd_hash[7:14])
    des_new = DES.new(key, 1)  # 1 is ECB_MODE
    response2 = des_new.encrypt(plain)

    if response1 == cipher[:8] and response2 == cipher[8:16]:
        return password

def check_password_map_mp(password):

    pwd_hash = nt_password_hash(password)

    key = des.expand_des_key(pwd_hash[:7])
    des_new = DES.new(key, 1)  # 1 is ECB_MODE
    response1 = des_new.encrypt(g_plain)

    key = des.expand_des_key(pwd_hash[7:14])
    des_new = DES.new(key, 1)  # 1 is ECB_MODE
    response2 = des_new.encrypt(g_plain)

    if response1 == g_cipher[:8] and response2 == g_cipher[8:16]:
        return password

def check_tail(list_chunk, plain, cipher):
    for num in list_chunk:
        hash_value = b''.join((num.to_bytes(2, 'little'), b'\x00'*5))
        key = des.expand_des_key(hash_value)
        des_new = DES.new(key, 1)# 1 is ECB_MODE
        response3 = des_new.encrypt(plain)
        if response3 == cipher[16:]:
            return hash_value

def check_tail_for_queue(args):

        hash_value = b''.join((args[0].to_bytes(2, 'little'), b'\x00'*5))

        key = des.expand_des_key(hash_value)
        des_new = DES.new(key, 1)  # 1 is ECB_MODE
        response3 = des_new.encrypt(args[1])
        if response3 == args[2][16:]:
            return hash_value

def _init(plain, cipher):
    """ Each pool process calls this initializer. Load the array to be populated into that process's global namespace """
    global g_plain, g_cipher
    g_plain = plain
    g_cipher = cipher

def check_tail_for_map(num):

    hash_value = b''.join((num.to_bytes(2, 'little'), b'\x00'*5))

    key = des.expand_des_key(hash_value)
    des_new = DES.new(key, 1)  # 1 is ECB_MODE
    response3 = des_new.encrypt(g_plain)
    if response3 == g_cipher[16:]:
        return hash_value

#divide data on chunks
def divide(list_, chunks=1):
    n = len(list_) // chunks
    i = -1
    for i in range(chunks-1):
        yield list_[i*n: (i+1)*n]
    yield list_[(i+1)*n:len(list_)]
   
class Worker():
    def __init__(self, workers, initargs):
        self.pool = Pool(processes=workers)
        self.workers = workers
        self.plain = initargs[0]
        self.cipher = initargs[1]
        self.result = False

    def callback(self, result):
        if result:
            self.pool.terminate()
            self.result = result
            
    def do_job(self, function, list_):
        for list_chunk in divide(list_, self.workers):
            self.pool.apply_async(function, args=(list_chunk, self.plain, self.cipher), callback=self.callback)
        self.pool.close()
        self.pool.join()

def worker_zmq(plain, cipher, value):
    context = zmq.Context()
    work_receiver = context.socket(zmq.PULL)
    work_receiver.connect("tcp://127.0.0.1:5556")

    for num in iter(work_receiver.recv_json, 'STOP'):
        result = check_tail_for_queue((num, plain, cipher))
        if(result!= None):
            value.append(result)
            break

def worker_queue(input, output):
    for args in iter(input.get, 'STOP'):
        result = check_tail_for_queue(args)
        if (result != None):
            output.put(result)
            break
    output.put(False)

def slice_(file, N):
    while True:
        chunk = tuple(islice(file, N))
        if not chunk:
            return
        yield chunk

class Spinner:
    busy = False
    delay = 0.1

    @staticmethod
    def spinning_cursor():
        while 1: 
            for cursor in '|/-\\': yield cursor

    def __init__(self, delay=None):
        self.spinner_generator = self.spinning_cursor()
        if delay and float(delay): self.delay = delay

    def spinner_task(self):
        while self.busy:
            sys.stdout.write(next(self.spinner_generator))
            sys.stdout.flush()
            time.sleep(self.delay)
            sys.stdout.write('\b')
            sys.stdout.flush()

    def start(self):
        self.busy = True
        threading.Thread(target=self.spinner_task).start()

    def stop(self):
        self.busy = False
        time.sleep(self.delay)

def crack_password(item, flag, manager, input_dir_name):
 
    start = time.time()
    
    '''
    First part of program
    brute forces last third of 
    21 byte password_hash
    '''
    #print("First part of program brute forces last third of 21 byte password_hash...")

    plain = bytes.fromhex(item['chal_hash_8byte'])
    cipher = bytes.fromhex(item['nt_response_24byte'])
    num_process_ = flag['num_process']

    
    '''
    Brute forcing values for last 7 bytes of 21 byte nt_password_hash from 0 to 65535(last 5 bytes are 0)
    hash_value -> first 2 bytes from those 7 bytes
    '''
    start_b = time.time()

    if flag['brute_force'] is 'Map':
        hash_value = False
        p1 = Pool(num_process_, initializer=_init, initargs=(plain, cipher))
        results = p1.map(check_tail_for_map, range(0, 65536))
   
        p1.close()
        p1.join()
 
        for i in results:
            if i != None:
                hash_value = i.hex()[0:4]
        if not hash_value:
            print("Wrong challenge response pair ", item['chal_hash_8byte'], " ", item['nt_response_24byte'])
            return

    if flag['brute_force'] is 'ZMQ':
        return_value = manager.list()
        context = zmq.Context()
        ventilator_send = context.socket(zmq.PUSH)
        ventilator_send.bind("tcp://127.0.0.1:5556")
        p = []
        for i in range(num_process_):
            p.append(Process(target=worker_zmq, args=(plain, cipher, return_value)))
        for i in range(num_process_):
            p[i].start()
        for num in range(0, 65536):
            ventilator_send.send_json(num)  # Write numbers into the queue
        for i in range(num_process_):
            ventilator_send.send_json('STOP')

        for i in range(num_process_):
            p[i].join()
        if len(return_value):
            hash_value = return_value[0].hex()[0:4]
        else:
            print("Wrong challenge response pair ", item['chal_hash_8byte'], " ", item['nt_response_24byte'])
            return

    if flag['brute_force'] is 'Queue':
        # Create queues
        task_queue = Queue()
        done_queue = Queue()
        # Submit tasks
        for num in range(0, 65536):
            task_queue.put((num, plain, cipher))
        # Start worker processes
        p = []
        for i in range(num_process_):
            p.append(Process(target=worker_queue, args=(task_queue, done_queue)))
        for i in range(num_process_):
            p[i].start()
            # Tell child processes to stop
        for i in range(num_process_):
            task_queue.put('STOP')

        result = done_queue.get()

        for i in range(num_process_):
            p[i].join()
        if result:
            hash_value = result.hex()[0:4]
        else:
            print("Wrong challenge response pair ", item['chal_hash_8byte'], " ", item['nt_response_24byte'])
            return

    if flag['brute_force'] is 'Async':
        w1 = Worker(num_process, initargs=(plain, cipher))  # parallel on num_processes cores
        w1.do_job(check_tail, range(0, 65536))
        if w1.result:
            hash_value = w1.result.hex()[0:4]
        else:
            print("Wrong challenge response pair ", item['chal_hash_8byte'], " ", item['nt_response_24byte'])
            return

    end_b = time.time()
    #print("First part of program execution time: ", end_b - start_b, "s\n")

    '''
    Second part
    1) Loads data from file {hash_value}.json to get list of passwords 
    which have matching last 7 bytes for their nt_password_hash.
    2) Finding a matching password for a given handshake(challenge and response)
    '''
    start_m = time.time()
    FILE_DIR = os.path.dirname(os.path.realpath('__file__'))
    INPUT_DIR = os.path.join(FILE_DIR, '{input_dir_}'.format(input_dir_=input_dir_name))
    os.makedirs(INPUT_DIR, exist_ok=True)
    filename = os.path.join(INPUT_DIR, '{key}.json'.format(key=hash_value))

    final_result = False

        #print("Second part of program- finding a match for a given handshake...")


    if flag['match'] is 'Seq':
        with open(filename, encoding="utf8") as input_file:
            for line in input_file:
                list_key_is_hash_value = json.loads(line)
                #????check in which a case is better to have multiple processes so that IPC is not overkill
                result = next((password for password in list_key_is_hash_value if check_password(password, plain, cipher)), None)
                if result:
                    print("Password is ", result)
                    final_result = result
                    break

    if flag['match'] is 'Map':
        with open(filename, encoding="utf8") as input_file:
            list_key_is_hash_value = [json.loads(line) for line in input_file]
            list_key_is_hash_value = [item for sublist in list_key_is_hash_value for item in sublist]  # to flatten lists
            p2 = Pool(num_process, initializer=_init, initargs=(plain, cipher))
            results = p2.map(check_password_map_mp, list_key_is_hash_value)
            p2.close()
            p2.join()
            for i in results:
                if i != None:
                    final_result = i
                    print("Password is ", i)
                    break

    if flag['match'] is 'Async':
        with open(filename, encoding="utf8") as input_file:
            list_key_is_hash_value = [json.loads(line) for line in input_file]
            list_key_is_hash_value = [item for sublist in list_key_is_hash_value for item in sublist]  # to flatten lists
            w = Worker(num_process, initargs = (plain, cipher))#parallel on num_processes cores
            w.do_job(check_password_async_mp, list_key_is_hash_value)
            if w.result:
                print("Password is ", w.result)
                final_result = w.result


    if not final_result:
        print("Password was not found!")
        index = "Not in list"
    else:
        index = list_key_is_hash_value.index(item['password'])
    end_m = time.time()
    #print("Second part of program execution time: ",end_c - start_c,"s\n")

    end = time.time()
    print("Program execution time: ", end - start, "s")



    item.update({"result" : final_result, "time" : end - start, "size_key_list" : len(list_key_is_hash_value), 
                 "index" : index, "time_bf" : end_b - start_b,
                  "time_m" : end_m - start_m}) #"time_r" : end_r - start_r,
    return item
              

if __name__ == '__main__':

    freeze_support()
    # DICT = "cleanfile.txt"##For testing purposes, to get random sample of passwords
    files = fnmatch.filter(os.listdir('.'), 'dictionary*.txt') #For testing purposes, to get random sample of passwords
    SAMPLES = 5
    SIZE = 7000000
    OUTPUT_DIR_NAME = 'ResultDictionaryNew'
    INPUT_DIR_NAME = 'DictionaryFiles'
    #INPUT_DIR_NAME = 'DictDir'
    #########################################################################################

    FILE_DIR = os.path.dirname(os.path.realpath('__file__'))
    OUTPUT_DIR = os.path.join(FILE_DIR, '{output_dir}'.format(output_dir=OUTPUT_DIR_NAME))
    os.makedirs(OUTPUT_DIR, exist_ok=True)
    #number of processes
    num_process = cpu_count()
    manager = Manager()
    rand_passwords=[]
    seed = 170
    print("Loading dictionaries...")
    # for file in files:
    #     try:
    #         with open(file, 'r', encoding='utf-8') as f:
    #             #for data in slice_(file, SIZE):
    #                 data = f.readlines()
    #                 random.seed(seed)
    #                 seed += 1
    #                 rand_passwords.extend(random.sample(data, SAMPLES))
    #                 print(rand_passwords)
    #                 del data[:]
    #     except IOError as error:
    #         print(error)
    #         os._exit(1)

    # try:
    #     with open("RandomPasswords.txt", 'w', encoding='utf-8') as f:
    #         str_ = json.dumps(rand_passwords)
    #         f.write(str_)
    # except IOError as error:
    #     print(error)
    #     os._exit(1)

    try:
        with open("RandomPasswords.txt", 'r', encoding='utf-8') as f:
                rand_passwords=json.load(f)
    except IOError as error:
        print(error)
        os._exit(1)
    print(rand_passwords)


    '''
        First Part
        Brute forcing values for last 7 bytes of nt_password_hash from 0 to 65535(last 5 bytes are 0)
        hash_value -> first 2 bytes from those 7 bytes
        
        Possible options:
        num_process -> number of processes for multiprocessing
        brute_force -> ['Map', 'Queue', 'ZMQ', 'Async']
        Map -> using multipocessing.Pool.map
        ZMQ -> using ZMQ
        Queue -> using Queue
        Async -> using multiprocessing.Pool.apply_async
    '''
    '''
        Second Part
        
        1) Loads data from file {hash_value}.json to get list of passwords 
        which have matching last 7 bytes for their nt_password_hash.
        Finding a matching password for a given handshake(challenge and response)
        
        Possible options:
        num_process -> number of processes for multiprocessing
        check -> ['Map', 'Seq', 'Async']
        Map -> using multipocessing.Pool.map
        Seq -> sequential - using list comprehension to check passwords until the match is found
        Async -> using multiprocessing.Pool.apply_async
    '''

    labels = []

    # flags = [{'num_process': num_process, 'brute_force': 'Queue', 'match': 'Seq'},
    #          {'num_process': num_process, 'brute_force': 'ZMQ', 'match': 'Seq'},
    #          {'num_process': num_process, 'brute_force': 'Map', 'match': 'Seq'},
    #          {'num_process': num_process, 'brute_force': 'Async', 'match': 'Seq'},
    #          {'num_process': num_process, 'brute_force': 'Async', 'match': 'Async'},
    #          {'num_process': num_process*2, 'brute_force': 'Async', 'match': 'Async'},
    #          {'num_process': num_process, 'brute_force': 'Async', 'match': 'Map'},
    #          {'num_process': num_process*2, 'brute_force': 'Async', 'match': 'Map'},
    #         ]

    flags = [{'num_process': num_process, 'brute_force': 'Async', 'match': 'Seq'},
             {'num_process': num_process, 'brute_force': 'Async', 'match': 'Async'}]

    for flag in flags:
        labels.append("BF{bf}({num}proc)Match{match}".format(bf=flag['brute_force'],
                      num=flag['num_process'], match= flag['match']))



    for label, flag in zip(labels, flags):
        peer_responses_plus_passwords = [(peer_response_challenge(password[:-1])) for password in rand_passwords]
        password_cracking_data = [crack_password(item, flag, manager,INPUT_DIR_NAME) for item in peer_responses_plus_passwords]
        # peer_responses_plus_passwords = [peer_response_challenge("EuropeR08")]
        # password_cracking_data = [crack_password(item, flag, manager,INPUT_DIR_NAME) for item in peer_responses_plus_passwords]
        # # password_cracking_data = [crack_password({"nt_response_24byte": "35bb69d5634beccb50d4fe62e87a1a3e7e3f29780f6438f2",
        # #     "chal_hash_8byte" : "0b8cf7f0cf8dca7e", "password": "proba"}, flag, manager,INPUT_DIR_NAME)]
        try:
            filename = os.path.join(OUTPUT_DIR, '{label}.json'.format(label=label))
            with codecs.open(filename, 'w','utf8') as f:
                str_ = json.dumps(password_cracking_data, separators=(',', ': '), ensure_ascii=False)
                f.write(str_)
        except IOError as error:
            print(error)
            os._exit(1)

    #######################################################################################################
    spinner = Spinner()
    spinner.start()
    # INPUT_DIR, labels, key, y_axis_label, y_down_lim, y_up_lim, *args -> x key, x_down_lim, x_up_lim, x_axis_label
    print("Ploting graphs...\n")
    graph(OUTPUT_DIR, labels, "time", "Time", 0, 2.5)
    graph(OUTPUT_DIR, labels, "time_bf", "Brute Force",  0, 2)
    graph(OUTPUT_DIR, labels, "time_m", "Match Password", 0, 1)
    #graph(OUTPUT_DIR, labels, "time_m", "Match Password", 0, 2, "index", 0, 240, "Index")
    #graph(OUTPUT_DIR, labels, "time_r", "Read Dict", 0, 0.004)
    spinner.stop()
    
